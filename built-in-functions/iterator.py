"""
Iterator:
	1. Related built-in functions: iter(), next()
	2. Required methods to be an iterable: __iter__(), __next__()
	3. The underlying behavior of for statement:
		It calls iter() and next() function while looping over a container object.
	4. Related exception: StopIteration
"""

# https://docs.python.org/3/tutorial/classes.html#iterators
import sys

for e in [1, 2, 3]:
    print(e)

for e in (1, 2, 3):
    print(e)

for e in {'one': 1, 'two': 2}:
    print(e)

for c in '123':
    print(c)

for line in open('test.txt'):
    """
    File is also a form of iterator.
    """
    print(line, end='')

"""
This style of access is clear, concise, and convenient.
The use of iterators pervades and unifies Python. Behind the scenes,
the for statement calls iter() on the container object.
The function returns an iterator object that defines the method __next__()
which accesses elements in the container one at a time.
When there are no more elements, __next__() raises a StopIteration exception
which tells the for loop to terminate. You can call the __next__()
method using the next() built-in function; this example shows how it all works:
"""
print('\n')
s = 'abc'

it = iter(s)

while True:
    try:
        print(next(it))
    except:
        print(sys.exc_info()[0])  # <class 'StopIteration'>
        break


class Reverse:

    """Iterator for looping over a sequence backwards. """

    def __init__(self, data):
        self.data = data
        self.index = len(data)

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

rev = Reverse('spam')
r = iter(rev)

while True:
    try:
        print(next(r))
    except:
        print(sys.exc_info()[0])  # <class 'StopIteration'>
        break

rev2 = Reverse('google')
for c2 in rev2:
    print(c2)

print('The following is not printing.')
for c3 in rev:
    print(c3)
print('Still not printing. Because the index aleadt reached to 0.')
print(rev.__dict__)
