It seems that how to make C/C++ extensions is less important that understanding the underlying intention why to use these mechanism. For the steps to make one, please refer to the official site.
<p/>
 * [Extending Python with C or C++][1]
 * [Python/C API Reference Manual][6]
 * [The Python Standard Library][7]

# Why use C/C++ extension
 * Due to concerns about performance, a lot of libraries are adopting extensions such as [**numpy**][2]. Definitely, Python's looping mechanism is quite slow to tackle massive data processing. Reportedly, it is 10 times slow than C's for loop.
 * Hiding algorithm, implementation or whatever it is called can be accomplished by this extending.
 * In order to make use of existing C/C++ libraries, a simple wrapper can be written as Python extension.

 > NOTE: [SWIG][3], [Boost.Python][4] is quite useful tool to have a grasp on

# How to make C or C++ Extension
 1. Set the path of header file directory and library directory. Take the following as an example of Windows.
   * Header file path
```
C:\Users\user\AppData\Local\Programs\Python\Python35-32\include
```
   * Library path
```
C:\Users\user\AppData\Local\Programs\Python\Python35-32\libs
```
 2. Set the extension of the executable file as <code>.pyd</code>
 3. Include header file in your c file and write some codes.
```
#include "python.h"
<some-code>
blah blah blah
```
 4. Finally, compile the written code <strong>in release mode</strong>.

# How Python and C/C++ communicate one another
The following functions are the essence in understanding the underlying behaviour. What is noteworthy is that they are using variable arguments.
 * PyArg_ParseTuple()
 * Py_BuildValue()

For more in detail, just read [the official guide for this][5].

# Sample Code

 * in C

Just a simple C code to get the length of a given string take place below.
```c
#include "python.h"

static PyObject *

sample_strlen(PyObject *self, PyObject *args)
{
    const char* str = NULL;
    int len = 0;

    if (!PyArg_ParseTuple(args, "s", &str))
         return NULL;

    len = strlen(str);

    return Py_BuildValue("i", len);
}

static PyMethodDef sample_methods[] = {
  {"strlen",
  sample_strlen, METH_VARARGS,
  "get the length of the given string."},
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef sample_module = {
  PyModuleDef_HEAD_INIT,
  "sample",
  "sample module",
  -1,
  sample_methods
};

PyMODINIT_FUNC
PyInit_sample(void) // The name of this function has to match to the name of python module.
{
    return PyModule_Create(&sample_module);
}
```

 * in Python
```python
import sample
length = sample.strlen('''They were successful
to get a global constituency
for releasing the prisoner of conscious
in Ethiopia.''')
print(length)
```

[1]: https://docs.python.org/3.5//extending/extending.html#a-simple-example
[2]: http://www.numpy.org/
[3]: http://www.swig.org/
[4]: http://www.boost.org/doc/libs/1_59_0/libs/python/doc/
[5]: https://docs.python.org/3.5/c-api/arg.html
[6]: https://docs.python.org/3.5/c-api/index.html
[7]: https://docs.python.org/3/library/
