[Python Basic Operators - TutorialsPoint][0]
[Standard operators as functions][1]

# Arithmatic operations

```python
a = 10 + 3 * 2
print( a )
s1 ="abc"
s1 = s1*3
# s2 = s1 + "def"
print( s1 )
myList =[10,20,30]*3
# myList = myList +[40,50,60]
print( myList )

c = 10/3
print( c)
print( type(c) )
d = 10 // 3
print( d )
e = 3**3
print( e )
```

# Boolean
```python
d = True
e = d + 3 # True is 1, False is 0
```

```python
c = 5
d = 0 < c < 10
```
In Python, this expression is valid, whereas in Java this invokes a compile time error and in C it can be written but has a different meaning.

# Operator <code>is</code> and <code>in</code>
### is
See if two values are identical.
```python
a = 100
b = 100
print(id(a)) # &a

print(a is b)
```
From the above example, a and b refer to the same object. By this, we can notice that Python has a distinct policy compared to traditional programming languages.

### in
By writing like follows, a value can be examined if it is included in a certain data type of object.
```python
someList = [10, 20]
print(10 in someList)
```

# Ternary operator
It appears there is no ternary operator in Python, but there is.

```python
b = 10 if a > 0 else 20
print(b)
```
Cool?

> NOTE: Not only for numbers, but also for sequential data types like str or list, + and * operator can be used to add or multiply

> NOTE!! unary operator ++, -- are not available in Python

```python
a = 10
++a
```
This doesn't emit any error, but is invalid.

[0]: http://www.tutorialspoint.com/python/pdf/python_basic_operators.pdf
[1]: https://docs.python.org/3/library/operator.html
