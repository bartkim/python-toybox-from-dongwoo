Before dive into the data types of Python, let's talk about ['variables'][a]. From python3, all the variables are consists of unicode character set. This is quite interesting subject to deal with. However, this will be discussed later.

# Interesting fact about number
```python
a = 123456789012345678901234567890
```
Unlike C or C++, the above is possible. Traditional language like C will secure 4 bytes, for example, to store the number, and then put the number into a variable. The above expression results in losing precision on the number. However, in Python, all the variables are an object. Python interpreter will silently create an object and then allocated a name to it. That is the main difference between dynamic languages and static languages.

# Bytes
```
b = b'link the poor people with road networks'
```
In socket communication, byte are quite useful data type to adopt.

# Tuple
Frequently used as a return data of a function or an argument. The most interesting fact is that tuple is immutable variable and contains variable types of data.
```python
t = (10, 20, 30)
```
```python
t = (10, 20, 30, 40, 50, 60, 70)
t[0] = 100
```
This doesn't run since tuple is immutable. The following exception is emitted.
```
Traceback (most recent call last):
  File "D:\strunk\python-intimediate\examples\test\complex_data_type.py", line 29, in <module>
    t[0] = 100
TypeError: 'tuple' object does not support item assignment
```
```python
try:
    t[0] = 100
except TypeError as e:
    print(e)
```

# List
```python
myList = [10, 20, 30]
print(myList[0])
print(myList[-1])
```
Reverse index starts from -1, whereas normal index starts from 0. This is quite confusing from time to time.

### Slicing(also valid in str or tuple)
```python
myList = [10, 20, 30, 40, 50, 60, 70]
print(myList[0])
print(myList[-1])
print(myList[1:3])
print(myList[1:5:2]) # incremental step is defined.
print(myList[:5:2])
print(myList[::2])
print(myList[-1:-4:-1]) # reverse slicing needs revers incremental steps.
print(myList[-1::-1])
```

Result
```
10
70
[20, 30]
[20, 40]
[10, 30, 50]
[10, 30, 50, 70]
[70, 60, 50]
[70, 60, 50, 40, 30, 20, 10]
```

Slicing stops at just before the last index. In other functions, excluding the last index is often seen.

# Dictionary
Dictionary has a binary tree, seemingly a red-black tree, to store data. In addition to it, a dictionary with character string key has a special way to print out like below.

```python
std ={'name':'aaa', 'age': 30}
print("name:%(name)s age:%(age)d" % std )
```

# Set
Set doesn't allow to duplicate its data.
```python
g = { 1, 2, 3, 4, 5, 6, 6, 6, 6}
print(g)
```

Result
```
{1, 2, 3, 4, 5, 6}
```

# Immutable and Mutable data types
str, tuple and frozen sets are the popular immutable data types

# Casting
Get a hint from the name of data types. Each function that has the name of data types can change the type of data such as int(), list(), float() and so forth.

# Useful built-in functions regarding data types
 * type()
 * len()
 * dir()
 * int()
 * list()
 * etc.

# How to organize a string to print out
 1. format() in str class
```python
print('a=%10d f=%.2f' % (a, f))
```

 NOTE: This is a better way to prevent inconsistency in the type.

 2. using %
```python
print('a={0:10} f={1:.2f}'.format(a, f))
```

NOTE: See the following page to understand more about python data types.<br/>
[Python Data Types][b]


> int, str, float, bool, complex, list, tuple, set, dict


[a]: http://www.learnpython.org/en/Variables_and_Types
[b]: https://docs.python.org/3/library/datatypes.html
