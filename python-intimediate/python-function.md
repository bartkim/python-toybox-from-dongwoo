# Duck typing

# functions without return statement
They return None

# Call by Object Reference
If arguments are immutable, Call by Object Reference occurred

# Call by Sharing
Mutable

# Scope Rule
LEGB

 * Local
 * Enclosing Function Local
 * Global
 * Built-in

# <code>\*args</code>
a tuple argument

# <code>\*\*kwargs</code>
a dictionary argument

# Example
```python
def divisor(a):

    ret = []

    for x in range(1, a + 1):
        if not a % x:
            ret.append(x)

    return ret

def enhanced_divisor(a):
    return [i for i in range(1, a + 1) if not a % i]

print(divisor(10))
print(enhanced_divisor(90))
```
