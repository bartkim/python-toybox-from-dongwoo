# class
When it comes to object oriented programming, please come up with APIE and [SOLID][3] concept. Those principles are quite important.
<p/>
Aside from that, Python's class is quite interesting, because they don't have function overloading and keywords for accessing member variables. Unlike the traditional languages like C++, it has a number of magic member methods, I found it quite useful. You can read the following article to get the hang of these.

 * [OCP][4]: Open for extending its usage, but closed to the modification of already existing code.

[A Guide to Python's Magic Methods][1]
<p/>
Of course, the magic methods include constructors, methods for memory allocation, how to get attributes and so on.

# Identity of instances
I wrote a simple code to see the instances created are identical. The result is negative.
```python
class T():
    def __init__(self):
        print(id(self))

if __name__ == '__main__':
    a = T()
    b = T()
```

# Encapsulation
As we access member variables via member methods, we can secure the consistency of the variable. Say that we want to store data information in a variable. Without any method or property, a value over 31 can be set in this variable, because Python has no regulation to access any member variables. For this regard, the concept of property has been emerged.

# Abstract Class
```python
class WantToBeAbstract:
  def abstract():
    pass
```
<p/>
Or,
<p/>
```python
class WantToBeAbstract:
  def abstract():
    raise NotImplementedError
```
In my opinion, raising an exception would be the better design because it forces to implement a concrete class and method.

<p/>
For more in detail:<p/>
[NotImplementedError][5]<br/>
[USAGES OF NOTIMPLEMENTED AND NOTIMPLEMENTEDERROR IN PYTHON][6]

# Inheritance
First of all, is it possible to upcast an object to its parent class in Python? No. In Python, that kind of concept doesn't exist.
<p/>
There are several ways to call the constructor in a superclass, but most of the case, the following method is preferred.
```python
class Employee:
    def __init__(self, name, addr):
        self.name= name
        self.addr = addr   

class Permanent(Employee):
    def __init__(self, name, addr, pay):
        super(Permanent, self).__init__(name, addr)
        self.pay = pay
```

For more in detail:
<p/>
[Inheritance Versus Composition][7]

### Other subject you might want to have a in-depth understanding
 * Iterator
 * Generator
 * Callable Object
 * Reflection
 * Context Management
 * Deep Copy and Shallow Copy
 * Pickling
 * Static methods
 * Class methods
 * Abstract methods

For more in detail:
<p/>
[How to use static, class or abstract methods in Python][2]


# List of magic methods

### Object creation and destruction
```python
__new__(cls, ...)
__init__(self, ...)
__del__(self)
```

### Comparison
```python
__eq__(self, other)
__ne__(self, other)
__lt__(self, other)
__le__(self, other)
__gt__(self, other)
__ge__(self, other)
```

### Misc
```python
__nonzero__(self)
__subclasses__(self)
__call__(self, ...)
__hash__(self)
__dir__(self)

```

### Mapping and giving sequential order
```python
__getitem__(self, index)
__setitem__(self, index, value)
__delitem__(self, index)
__len__(self)
__contains__(self, item)
__iter__(self)
__reversed__(self)
__missing__(self, key)
__length_hint__(self)
```

### Conversion
```python
# to string
__repr__(self)
__str__(self)
__unicode__(self)

# to a specific type
__complex__(self)
__int__(self)
__long__(self)
__float__(self)
__oct__(self)
__hex__(self)
__index__(self)
```

### Attribute Access
```python
__getattr__(self, name)
__setattr__(self, name, value)
__delattr__(self, name)
__getattribute__(self, name)
```

### Arithmetic Operation
```python
__add__(self, other)
__sub__(self, other)
__mul__(self, other)
__floordiv__(self, other)
__div__(self, other)
__truediv__(self, other)
__mod__(self, other)
__divmod__(self, other)
__pow__(self, other[, modulo])
__lshift__(self, other)
__rshift__(self, other)
__and__(self, other)
__xor__(self, other)
__or__(self, other)

# Unary
__neg__(self)
__pos__(self)
__abs__(self)
__invert__(self)
```
### Context Manager
```python
__enter__(self)
__exit__(self, exc_type, exc_val, exc_tb)
```

### Descriptor
```python
__get__(self, instance, owner)
__set__(self, instance, value)
__delete__(self, instance)
```

[1]: http://www.rafekettler.com/magicmethods.html
[2]: https://julien.danjou.info/blog/2013/guide-python-static-class-abstract-methods
[3]: https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)
[4]: https://en.wikipedia.org/wiki/Open/closed_principle
[5]: https://docs.python.org/3/library/exceptions.html#NotImplementedError
[6]: http://dwieeb.com/2015/06/02/usages-of-notimplemented-and-notimplementederror-in-python/
[7]: http://learnpythonthehardway.org/book/ex44.html
