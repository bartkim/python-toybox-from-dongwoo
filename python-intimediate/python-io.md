# input()
### 2.x
 * input(): returns int
 * raw_input(): returns str

### 3.x
Unlike 2.x, input returns str and raw_input() was integrated with input()
