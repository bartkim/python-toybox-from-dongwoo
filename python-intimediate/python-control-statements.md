# No switch statement
In the advent of dictionary, switch statement can be written like follows

```python
k = { 7 : 'a', 8 : 'b', 9 : 'c' }
print(k[int(input())//10])
```

# Packing and Unpacking
```python
a = 10, 20, 30
```
This converted into a tuple, so <code>a</code> becomes (10, 20, 30)

```python
a, b, c = (10, 20, 30)
```
This tuple breaks down into a simple int that respectively assigned to the above variables.

# <code>for in</code> and a dict
While a <code>for in</code> statement runs with a dictionary, it give a key in every round.

```python
k = {'1': 'a'}
for k, v in k.items():
  print(k, v)
```
k.items() returns a tuple every for statement and it was unpacked to k and v.

# range()
```python
for n in range(0, 10, 1):
  print(n)
```

# <code>for in</code> and <code>else</code>
```python
for n in range(0, 10, 1):
  if n == 5:
    break
  print(n)
else:
  print('else')
```

Unless a for loop is stopped by a break, <code>else</code> statement runs.

# <code>for in</code> and list
```python
l = [n * 3 for n in range(0, 21, 2) if n % 2 == 0]
```
This expression comed in quite handy when you want to generating a list.
