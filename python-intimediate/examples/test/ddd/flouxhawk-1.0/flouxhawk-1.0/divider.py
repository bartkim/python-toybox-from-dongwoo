def divisor(a):
    
    ret = []
    
    for x in range(1, a + 1):
        if not a % x:
            ret.append(x)
            
    return ret

def enhanced_divisor(a):
    return [i for i in range(1, a + 1) if not a % i]

print(divisor(10))
print(enhanced_divisor(90))
