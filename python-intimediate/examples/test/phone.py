class Phone(object):
    def feature(self):
        raise NotImplementedError

class FeaturePhone(Phone):
    def feature(self):
        print('The camera is very good.')
        
class SmartPhone(Phone):
    def feature(self):
        print('All feature are smart.')
        
class HomePhone(Phone):
    def feature(self):
        print('This phone is the center of a home.')
        
class IPhone(object):
    
    def __init__(self, p):
        self.phone = p
        
    def set(self, p):
        self.phone = p
        
    def feature(self):
        return self.phone.feature()

if __name__ == '__main__':
    phone = IPhone(FeaturePhone())
    phone.feature()
    
    phone.set(SmartPhone())
    phone.feature()
    
    phone.set(HomePhone())
    phone.feature()