class Employee:
    def __init__(self, name, addr):
        self.name= name
        self.addr = addr
    def print_name(self):
        print("name:", self.name)
 
    def print_addr(self):
        print("addr:", self.addr)
         
    def print_pay(self):
        pass

    def print_salary(self):
        pass
    
    def print_department(self):
        pass
   

class Permanent(Employee):
    def __init__(self, name, addr, pay):
        super(Permanent, self).__init__(name, addr)
        self.pay = pay
        
    def print_salary(self):
        print('pay', self.pay)
        

class Temporary(Employee):
    def __init__(self, name, addr, pay, days):
        super(Temporary, self).__init__(name, addr)
        self.pay = pay * days
        
    def print_pay(self):
        print('pay', self.pay)
        
class Manager(Employee):
    def __init__(self, name, addr, dep):
        super(Manager, self).__init__(name, addr)
        self.dep = dep
        
    def print_department(self):
        print('dep', self.dep)
    

if __name__ == '__main__':
    l = []
    l.append(Permanent('aaa', 'bbb', 1000))
    l.append(Temporary('zzz', 'xxx', 10, 50))    
    l.append(Manager('jjj', 'lll', 'ooo'))
    
    for e in l:
        e.print_name()
        e.print_addr()
        e.print_pay()
        e.print_salary()
        e.print_department()
        
        