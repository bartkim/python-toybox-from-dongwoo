s1 = "abc" \
"def"

s2 = """abc
    def
        ghi
            lmn"""
            
print(s1)
print(s2)

a = 10
f = 3.14

print('a=%10d f=%.2f' % (a, f))
print('a={0:10} f={1:.2f}'.format(a, f))

myList = [10, 20, 30, 40, 50, 60, 70]
print(myList[0])
print(myList[-1])
print(myList[1:3])
print(myList[1:5:2])
print(myList[:5:2])
print(myList[::2])
print(myList[-1:-4:-1])
print(myList[-1::-1])

t = (10, 20, 30, 40, 50, 60, 70)

try:
    t[0] = 100
except TypeError as e:
    print(e)


g = { 1, 2, 3, 4, 5, 6, 6, 6, 6}
print(g)