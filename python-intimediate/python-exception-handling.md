Actually as a C++ developer, I've barely used exception handling measures although the language provide strong supports for that. It is mainly because it slows down the performance. However, in Python, I don't really care about the drawbacks which come from exception handling.
<p/>
Read this interesting article about the cons of exception handling.
 * [Exception Handling Considered Harmful][1]

[1]: http://www.lighterra.com/papers/exceptionsharmful/
