```python
address_book = []

def print_menu():
    print('1. input', '2. print', '3. search', '4. exit', sep='\n')

def print_first_row():
    # print('--------------------------')
    # Small enhancement in shortening the source code
    print('-' * 25)
    print('{:6}{:6}{:10}'.format('Name', 'Age', 'Address'))
    # print('--------------------------')
    print('-' * 25)

def _input():

    name = input('name')
    age = int(input('age'))
    address = input('address')

    address_book.append({ 'Name': name, 'Age': age, 'Address': address})

    if input('continue?') == 'y':
        _input()

def _print():
    print_first_row()
    for x in address_book:
        print('%(Name)6s %(Age)6d %(Address)10s' % x)

def _search():
    name = input('name')
    print_first_row()

    # Is it an effective code?
    for item in [x for x in address_book if x['Name'] == name]:
        print('%(Name)6s %(Age)6d %(Address)10s' % item)

menu = {1: _input, 2: _print, 3: _search, 4: exit}

if __name__ =='__main__':
    while True:
        print_menu()
        try:
            menu[int(input('select:'))]()
        except:
            print('unavailable menu')
```
