# Features of Python
 1. a wide range of libraries(including third party ones)
 2. We can shorten the amount of code lines. Data structures as data type. C++11 also adopt tuple as a data type. Can you feel the difference between data type and data structure?
 3. develop first and enhance later. and it is very fast to develop a software.
 4. simple and easy syntax
 5. interpreter dependent language but there was improvements. can speed up the performance by using .pyc In other words, it can be compiled.
 6. platform independent language. Python interpreter.
 7. dynamically allocated data types.
 8. list, dictionary, tuple, etc.
 9. memory that automatically managed with reference count

# Broad usage of this fabulous language
The library name will be updated under the items respectively.
##### GUI
##### Web Apps
##### Network application
##### Database
Sqlite
##### Text
##### Calculation
##### Parallel programming

> In conclusion, there are nothing Python cannot do

# How to get Python Interpreter
Of course, Python can be downloaded from [its official website][1]. After downloading and installing, the console can be used to test if the interpreter has been successfully installed.

##### Testing
First off, Python has [built-in functions][2], [libraries to be imported][3] using 'import' directives and third party libraries. Run a python shell and put some codes like <code>print('blah')</code>. If you can see the result you are done with installing the interpreter. Or you can use built-in IDE whose name is IDLE.

##### NOTE: The location of python interpreter in Widows is like the following. It has been updated in Python 3.5
```
C:\Users\user\AppData\Local\Programs\Python\Python35-32

2015-10-19  오전 09:57    <DIR>          DLLs
2015-10-19  오전 09:57    <DIR>          Doc
2015-10-19  오전 09:57    <DIR>          include
2015-10-19  오전 09:57    <DIR>          Lib
2015-10-19  오전 09:57    <DIR>          libs
2015-09-13  오전 02:20            30,043 LICENSE.txt
2015-09-13  오전 01:06           282,293 NEWS.txt
2015-09-13  오전 02:17            38,168 python.exe
2015-09-13  오전 02:17            51,480 python3.dll
2015-09-13  오전 02:17         3,128,600 python35.dll
2015-09-13  오전 02:17            38,168 pythonw.exe
2015-09-13  오전 01:06             6,740 README.txt
2015-10-19  오전 09:57    <DIR>          Scripts
2015-10-19  오전 09:57    <DIR>          tcl
2015-10-19  오전 09:57    <DIR>          Tools
2015-06-25  오후 11:34            85,328 vcruntime140.dll
```

##### IDE
There are [eclipse][4] and [pyCharm][5] which are popular among python developers. I would prefer pyCharm, but during this course, we will be using Eclipse. In order to make yourself feel comfortable during writing python codes, you'd better to install ['PyDev'][6] through Eclipse Marketplace. The market is found under the 'Windows' menu in Eclipse. If you are a competent developer, I think you can manage to do the rest of the steps.


[1]: https://www.python.org/downloads/
[2]: https://docs.python.org/3.3/library/functions.html
[3]: https://docs.python.org/3/reference/import.html
[4]: http://www.eclipse.org/downloads/download.php
[5]:https://www.jetbrains.com/pycharm/
[6]: http://www.pydev.org/
