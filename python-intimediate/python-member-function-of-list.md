# <code>append()</code>
append an item. The size of a list is not limited to a certain amount.

# <code>count()</code>
returns the number of elements which are identical to an argument.

# <code>len()</code>
Python built-in function

# <code>clear()</code>
clean up the data in a list.

# <code>insert()</code>
```python
l.insert(1, 100)
```
insert 100 into <code>l[1]</code> and move the items after <code>l[1]</code> to the right.

# <code>remove()</code>
remove the given item from the argument in a list

# <code>pop()</code>
return the last item in a list and delete it.

# <code>del()</code>
Python built-in function

# <code>extend()</code>
```python
l.extend('aaa')
```
