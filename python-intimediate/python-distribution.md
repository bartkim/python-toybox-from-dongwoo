In the advent of **distutil** and **setuptools**, it became quite easy to make a dist.

# setup.py

## General information
Those information other than name and version can be omitted, but not encouraged to do that. More information, more reliable program.

 * name
 * version
 * description
 * author
 * author_email
 * url

## Needed to organize a dist
 * py_modules
 * packages
 * package_dir
 * package_data
 * data_files

## Howto
 * Source distribution
```
 python setup.py sdist
```
This command will produce some zip or tar.gz files. In the compressed file, all the source files are found. If the following command runs, unpacked files will be presented.

```
 python setup.py install
```
This command will produce some zip or tar.gz files which contain all the source code.

* Binary distribution

Unlike to source file distribution, if a C/C++ extension is included in the target project, binary distribution will be smarter way, because compiled outputs will be included in the target distributable by itself.
```
python setup.py bdist

# in Windows,
python setup.py bdist_wininst

# Also available to pack in an egg file.
# only if setuptools library is imported in setup.py
python setup.py bdist_egg
```

# Including third party library
With specified dependencies in **requirements.txt**, pip can automatically download and import the libraries in need.

 * [pip][4]
 * [Requirement Files][2]
 * [Requirements File Format][3]


# Making of executables
By using the following libraries, you can run a program without having python files.
 * py2exe
 * py2app
 * cx_Freeze


For more information, visit [here][1]
[1]: https://docs.python.org/3.5//distutils/index.html
[2]: https://pip.readthedocs.org/en/stable/user_guide/#requirements-files
[3]: https://pip.readthedocs.org/en/stable/reference/pip_install/#requirements-file-format
[4]: https://pip.readthedocs.org/en/stable/
