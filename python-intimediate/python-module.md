Module and Package is quite straightforward in Python. They are easy to understand. Take a look at the follows.

# Module

### import
In order to understand how Python imports modules, Please read [this guide][1] from the official Python document.
<p/>
By Using <code>from <a module> import <an object or object list></code> you can omit the module name.

```python
from module import something1, something2
```

### <code>dir()</code>
Please refer to [this official guide][3].

### sys.path
```python
sys.path.append('some path')

or

PYTHONPATH=<some path>
```

### sys.modules
contains information about loaded modules. Read [this][4].

### Compiled Python File and Module
Once you import a module, a compiled binary for the module is created and the extension will be <strong>.pyc</strong>. This is because loading a module is heavy work. The solution is compilation [beforehand][2].

> NOTE: You can also compile any module you want

```python
import py_compile
py_compile.compile('some_module.py')
```
<p>or</p>
In your command prompt
```
python -m py_compile <some_module.py>
```

> NOTE: To be honest, the libraries are already compiled into .pyc files.

### <code>\_\_name\_\_</code>
It can hold the name of a module or '\_\_main\_\_'. If a module enter itself to running state, then the value will be  '\_\_main\_\_'. You might get confused because Python doesn't have a main() function. Instead, Python provide \_\_name\_\_ to distinguish the entry point.

For more information, read these.
[Top-level script environment][5]
[A module's \_\_name\_\_][6]

### Qualified Name
A module has to do with a qualified name. It would be better to understand how it works. In summary, qualified name is nested in a module, so it can be found directly in the name space the module gives. On the contrary, a variable which isn't included in a module, a class or a function is under the rule of [LEGB][8] principle.
[Qualified name for classes and functions][7]

[1]: https://docs.python.org/3/reference/import.html
[2]: http://www.merriam-webster.com/dictionary/beforehand
[3]: https://docs.python.org/3/library/functions.html#dir
[4]: http://www.python-course.eu/sys_module.php
[5]: https://docs.python.org/3/library/__main__.html
[6]: http://www.ibiblio.org/g2swap/byteofpython/read/module-name.html
[7]: https://www.python.org/dev/peps/pep-3155/
[8]: https://blog.mozilla.org/webdev/2011/01/31/python-scoping-understanding-legb/

# Package
Packaging is just organizing structures of modules by using folders and \_\_init\_\_.py. Please read [the interesting article][9] about what to put in \_\_init\_\_.py. If you have time to read more, [this][10] might strengthen your knowledge about packaging.

[9]: https://www.reddit.com/r/Python/comments/1bbbwk/whats_your_opinion_on_what_to_include_in_init_py/
[10]: http://mikegrouchy.com/blog/2012/05/be-pythonic-__init__py.html
